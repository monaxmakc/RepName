#include "snake.h"

Snake::Snake()
{
	segments.push_back({ 3,3 });
	segments.push_back({ 2,3 });
	segments.push_back({ 1,3 });
}

void Snake::setDirection(Snake::Direction d)
{
	dir = d;
}

void Snake::Move()
{

	SnakeSegment s = segments.front(); // ������ int x = segments.front().x; int y = segments.front().y;

	if (dir == right)
		s.x += 1;
	else if (dir == left)
		s.x -= 1;
	else if (dir == up)
		s.y -= 1;
	else
		s.y += 1;

	segments.push_front(s);
	segments.pop_back();
}