#pragma once
#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <string>


namespace Color {
    constexpr SDL_Color RED = { 0xFF, 0, 0, 0xFF };
    constexpr SDL_Color BLACK = { 0, 0, 0, 0xFF };
    constexpr SDL_Color GREEN = { 0, 0xFF, 0, 0xFF };
} // namespace Color


/// ����� ��� ���������
class Painter {
public:
    Painter(SDL_Renderer* rend);

    /**
     * @brief ������ ���� ���������
     * @param color ����
     */
    void setColor(SDL_Color color);

    /// �������� (������ ��� ������� ������)
    void clear();

    /// ���������� ���������� ������
    void present();

    /**
     * @brief ������ ������������� ������� ������
     * @param x �
     * @param y �
     * @param w ������
     * @param h ������
     */
    void fillRect(int x, int y, int w, int h);

private:
    SDL_Renderer* rend;
};


struct EventHandler {
    virtual void onLeftButtonPressed(int x, int y, bool pressed) = 0;
    virtual void onKeyPressed(SDL_Scancode keyCode, bool pressed) = 0;
    virtual void onPaintEvent(Painter painter) = 0;
    virtual void onTickEvent(int timeDeltaMs) = 0;
};


class Window {
public:
    /**
     * @brief ����
     * @param title ���������
     * @param width ������
     * @param height ������
     */
    Window(std::string title, int width, int height);
    ~Window();

    /// ������
    int getWidth() const;

    /// ������
    int getHeight() const;

    /// ������
    void mainloop();

    /// �����
    void quit();

    /// ���������� ����������
    void setHandler(struct EventHandler* value);

private:
    SDL_Window* wnd = nullptr;
    SDL_Renderer* rend = nullptr;

    int w;
    int h;

    bool run = false;
    struct EventHandler* handler = nullptr;
};


Window::Window(std::string title, int width, int height)
    : w(width)
    , h(height)
{
    SDL_Init(SDL_INIT_EVERYTHING);

    wnd = SDL_CreateWindow(
        title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
    rend = SDL_CreateRenderer(wnd, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    SDL_SetRenderDrawColor(rend, Color::BLACK.r, Color::BLACK.g, Color::BLACK.b, Color::BLACK.a);
}


Window::~Window()
{
    if (rend)
        SDL_DestroyRenderer(rend);
    if (wnd)
        SDL_DestroyWindow(wnd);
    SDL_Quit();
}


int Window::getWidth() const
{
    return w;
}


int Window::getHeight() const
{
    return h;
}


void Window::mainloop()
{
    run = true;
    SDL_Event event;

    while (run) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                quit();
                break;

            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
                handler->onLeftButtonPressed(event.button.x, event.button.y, event.button.state == SDL_PRESSED);
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                handler->onKeyPressed(event.key.keysym.scancode, event.key.state == SDL_PRESSED);
                break;

            default:
                break;
            }
        }

        constexpr int delay = 10;

        handler->onPaintEvent(Painter{ rend });
        handler->onTickEvent(delay);

        SDL_Delay(delay);
    }
}


void Window::quit()
{
    run = false;
}

void Window::setHandler(EventHandler* value)
{
    handler = value;
}


Painter::Painter(SDL_Renderer* rend)
    : rend(rend)
{
}


void Painter::setColor(SDL_Color color)
{
    SDL_SetRenderDrawColor(rend, color.r, color.g, color.b, color.a);
}


void Painter::clear()
{
    SDL_RenderClear(rend);
}


void Painter::present()
{
    SDL_RenderPresent(rend);
}


void Painter::fillRect(int x, int y, int w, int h)
{
    SDL_Rect rect{ x, y, w, h };
    SDL_RenderFillRect(rend, &rect);
}
