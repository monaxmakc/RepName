#include "Painter.h"
#include "snake.h"
#include <iostream>
#include <utility>

std::pair<int, int> foodCheck();

class SnakeHandler : public EventHandler
{
public:
	SnakeHandler() = default;

	virtual void onLeftButtonPressed(int x, int y, bool pressed) override;

	virtual void onKeyPressed(SDL_Scancode keyCode, bool pressed) override;

	virtual void onPaintEvent(Painter painter) override;

	virtual void onTickEvent(int timeDeltaMs) override;

private:
	Snake snake;
};


void SnakeHandler::onLeftButtonPressed(int x, int y, bool pressed)
{

}

void SnakeHandler::onKeyPressed(SDL_Scancode keyCode, bool pressed)
{
	if (keyCode == 79)
		snake.setDirection(snake.right);
	else if (keyCode == 80)
		snake.setDirection(snake.left);
	else if (keyCode == 81)
		snake.setDirection(snake.down);
	else if (keyCode == 82)
		snake.setDirection(snake.up);
}

void SnakeHandler::onPaintEvent(Painter painter)
{
	painter.setColor(Color::BLACK);
	painter.clear();

	painter.setColor(Color::RED);
	int w = 50, h = 50; // ������ ��������� ������
	for (SnakeSegment segment : snake)
	{
		painter.fillRect(50 * segment.x + 1, 50 * segment.y + 1, w - 2, h - 2);
	}

	painter.fillRect(50 * foodCheck().first + 1, 50 * foodCheck().second + 1, w - 2, h - 2);

	painter.present();

	int snakeMoveTime = 500;
	int snakeMoveTimeMultiplicity = 1;
	// int snakeMoveTimeMultiplicity = 1 - ����� ����� (���-��_���������_������ / 6) / 10 ;
	SDL_Delay(snakeMoveTime * snakeMoveTimeMultiplicity); //�������� �������� ������
}

void SnakeHandler::onTickEvent(int timeDeltaMs)
{
	snake.Move();
}

// ����������� ��� ���� � snake.h - ������ �� 10�� ��� ���� ���������.

std::pair<int, int> foodCheck()
{
	// ��������� �������������� ���
	
	// �������� �� ������������� �������� - �� ��� ������, ���������� �� �� ���...
	//  while(bool create = false)
	//		int x = rand() % 16;
	//		int y = rand() % 16;
	//		while (bool check = false)
	//			for (SnakeSegment segment : snake)
	//				if (segment.x != x & segment.y != y)
	//					if... ��������� �������
	//						check = true;
	//		create = true;
	//

	int x = rand() % 16;
	int y = rand() % 16;


	return { x, y };
}


int main()
{
	Window window("Snake", 1024, 768);
	SnakeHandler handler;

	window.setHandler(&handler);
	window.mainloop();


	return 0;
}