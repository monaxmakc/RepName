#pragma once
#include <list>


struct SnakeSegment
{
	int x;
	int y;
};

class Snake {
public:
	Snake();

	enum Direction {left, right, up, down};

	void setDirection(Direction d);

	void Move();

	inline auto begin()
	{
		return segments.begin();
	}

	inline auto end()
	{
		return segments.end();
	}

private:
	std::list<SnakeSegment> segments;
	Direction dir = right;
};